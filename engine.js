var money = 0;
var pool = 0;
var moneyPerSecond = 0;
var stock = document.getElementById("stock");
var worker = document.getElementById("worker");
var machine = document.getElementById("machine");
var clicker = document.getElementById("dblClick");
var factory = document.getElementById("factory");
var printer = document.getElementById("printer");
var secret = document.getElementById("secret");
var increase = 1.8;

valueChecker();

function getMoney(){
  money++;
  money = money+pool;
  document.getElementById("money").innerHTML = "<img src='images/won.png'>"+money;
  document.getElementById("currentMoney").innerHTML = money;
  valueChecker();
}

function doubleMoney(){
  valueChecker();
  pool += 1;
  document.getElementById("money").innerHTML = "<img src='images/won.png'>"+money;
  document.getElementById("currentMoney").innerHTML = money;
  money -= clicker.value;
  clicker.value = Math.floor(+clicker.value * increase);
  clicker.childNodes[3].innerHTML= +clicker.value;
  valueChecker();
}

function moreMoney(){
  valueChecker();
  pool += 4;
  document.getElementById("money").innerHTML = "<img src='images/won.png'>"+money;
  document.getElementById("currentMoney").innerHTML = money;
  money -= stock.value;
  stock.value = Math.floor(+stock.value * increase);
  stock.childNodes[3].innerHTML= +stock.value;
  valueChecker();
}

function addTimeMoney() {
  valueChecker();
  moneyPerSecond += 1;
  money -= worker.value;
  document.getElementById("money").innerHTML = "<img src='images/won.png'>"+(money+=moneyPerSecond);
  document.getElementById("dps").innerHTML = moneyPerSecond+" dps";
  document.getElementById("currentMoney").innerHTML = money;
  worker.value = Math.floor(+worker.value * increase);
  worker.childNodes[3].innerHTML= +worker.value;
  valueChecker();
}

function addTimeMoolah() {
  valueChecker();
  moneyPerSecond += 10;
  money -= machine.value;
  document.getElementById("money").innerHTML = "<img src='images/won.png'>"+(money+=moneyPerSecond);
  document.getElementById("dps").innerHTML = moneyPerSecond+" dps";
  document.getElementById("currentMoney").innerHTML = money;
  machine.value = Math.floor(+machine.value * increase);
  machine.childNodes[3].innerHTML= +machine.value;
  valueChecker();
}
function addFactory(){
  valueChecker();
  moneyPerSecond += 15;
  money -= factory.value;
  document.getElementById("money").innerHTML = "<img src='images/won.png'>"+money;
  factory.value = Math.floor(+factory.value * increase);
  factory.childNodes[3].innerHTML= +factory.value;
}
function addPrinter(){
  valueChecker();
  pool += 20;
  money -= printer.value;
  document.getElementById("money").innerHTML = "<img src='images/won.png'>"+money;
  printer.value = Math.floor(+printer.value * increase);
  printer.childNodes[3].innerHTML= +printer.value;
}
function addSecret(){
  valueChecker();
  moneyPerSecond += 9001;
  money -= secret.value;
  document.getElementById("money").innerHTML = "<img src='images/won.png'>"+money;
  secret.value = Math.floor(+secret.value * increase);
  document.getElementById("dps").innerHTML = moneyPerSecond+" dps";
  document.getElementById("currentMoney").innerHTML = money;
  secret.childNodes[3].innerHTML= +secret.value;
}

function ticker() {
    document.getElementById("money").innerHTML = "<img src='images/won.png'>"+(money += moneyPerSecond);
    document.getElementById("currentMoney").innerHTML = money;
    valueChecker();
  }

function valueChecker() {
  if (money < clicker.value) {
    clicker.setAttribute("class", "disabled");
    clicker.removeAttribute("onClick", "doubleMoney()");
  } else {
    clicker.removeAttribute("class", "disabled");
    clicker.setAttribute("class", "haveCash");
    clicker.setAttribute("onClick", "doubleMoney()");
  }
  if (money < stock.value) {
    stock.setAttribute("class", "disabled");
    stock.removeAttribute("onClick", "moreMoney");
  } else {
    stock.removeAttribute("class", "disabled");
    stock.setAttribute("class", "haveCash");
    stock.setAttribute("onClick", "moreMoney()");
  }
  if (money < worker.value){
    worker.setAttribute("class", "disabled");
    worker.removeAttribute("onClick", "addTimeMoney()")
  } else {
    worker.removeAttribute("class", "disabled");
    worker.setAttribute("class", "haveCash");
    worker.setAttribute("onClick", "addTimeMoney()");
  }
  if (money < machine.value) {
    machine.setAttribute("class", "disabled");
    machine.removeAttribute("onClick", "addTimeMoolah()");
  } else {
    machine.removeAttribute("class", "disabled");
    machine.setAttribute("class", "haveCash");
    machine.setAttribute("onClick", "addTimeMoolah()");
  }
  if (money < factory.value) {
    factory.setAttribute("class", "disabled");
    factory.removeAttribute("onClick", "addFactory()");
  } else {
    factory.removeAttribute("class", "disabled");
    factory.setAttribute("class", "haveCash");
    factory.setAttribute("onClick", "addFactory()");
  }
  if (money < printer.value) {
    printer.setAttribute("class", "disabled");
    printer.removeAttribute("onClick", "addPrinter()");
  } else {
    printer.removeAttribute("class", "disabled");
    printer.setAttribute("class", "haveCash");
    printer.setAttribute("onClick", "addPrinter()");
  }
  if (money < secret.value) {
    secret.setAttribute("class", "disabled");
    secret.removeAttribute("onClick", "addSecret()");
  } else {
    secret.removeAttribute("class", "disabled");
    secret.setAttribute("class", "haveCash");
    secret.setAttribute("onClick", "addSecret()");
  }
}

document.getElementById("firstButton").addEventListener("click", function(e){
  var y = e.pageY-15+"px";
  var x = e.pageX+15+"px";
  var div = document.getElementById("particle");
  div.style.top=y;
  div.style.left=x;
  div.innerHTML=pool;
});

var buttons = document.getElementsByName("item");
for(var i = 0; i < buttons.length; i++){
  buttons[i].addEventListener("mouseover", showPrice, false);
}

function showPrice(e){
  var y = e.pageY+"px";
  var x = e.pageX+15+"px";
  var div = document.getElementById("particle");
  div.style.top=y;
  div.style.left=x;
  console.log(x+", "+y);
  div.innerHTML=this.value;
}
setInterval(ticker, 1000);